﻿using Microsoft.Win32;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Management;
using System.Threading;

namespace ACDPIFix
{
    internal class Program
    {
        const string dbPath = ".\\ac_paths.txt";
        static void Main(string[] args)
        {
            var acs = LoadACs();
            if (acs.Count < 1)
            {
                Console.WriteLine($"To manually specify the ACs to monitor then edit the file {dbPath}");
                var yn = YesNo($"Do you want to search your drives for ACs to monitor? Y/N");
                if (yn)
                {
                    acs = FindACs();
                    SaveACs(acs);
                }
            }
            if (acs.Count < 1)
            {
                Console.WriteLine("could not find any ACs");
            }
            else
            {
                Console.WriteLine($"monitoring {acs.Count} ACs");
                while (true)
                {
                    foreach (var ac in acs)
                    {
                        Fix(ac);
                    }
                    Thread.Sleep(1000);
                }
            }
            Console.WriteLine("strike any key to exit.");
            Console.ReadKey();
        }
        static void SaveACs(List<string> paths)
        {
            if (File.Exists(dbPath))
                File.Delete(dbPath);
            File.WriteAllLines(dbPath, paths);
        }
        static List<string> LoadACs()
        {
            if (!File.Exists(dbPath))
                return new List<string>();
            return File.ReadAllLines(dbPath).ToList();
        }
        static List<string> FindACs()
        {
            List<string> searched = new List<string>();
            List<string> acs = new List<string>();
            Console.WriteLine("finding ACs");
            while (true)
            {
                Console.WriteLine("strike the key of the drive to search, example: C");
                var driveLetter = Console.ReadKey(true).KeyChar.ToString().ToUpper();
                if (!"ABCDEFGHIJKLMNOPQRSTUVWXYZ".Contains(driveLetter))
                {
                    Console.WriteLine($"invalid drive {driveLetter}");
                    continue;
                }
                if (searched.Contains(driveLetter))
                {
                    Console.WriteLine($"already searched drive {driveLetter}");
                    continue;
                }
                try
                {
                    searched.Add(driveLetter);
                    var files = FindACs(driveLetter);
                    //var fNam = "acclient.exe";
                    //var srch = $"{driveLetter}:\\";
                    //Console.WriteLine($"searching {srch} for {fNam}");
                    //var files = Directory.EnumerateFiles(srch, fNam, SearchOption.AllDirectories);
                    acs.AddRange(files);
                    var yn = YesNo($"found {files.Count()} ACs, {acs.Count} total ACs, search another drive? Y/N");
                    if (!yn)
                    {
                        return acs;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"{ex.Message}");
                }
            }
        }
        static List<string> FindACs(string driveLetter)
        {
            List<string> acs = new List<string>();
            var fNam = "acclient.exe";
            var srch = $"{driveLetter}:\\";
            Console.WriteLine($"searching {srch} for {fNam}");

            string drive = $"\"{driveLetter}:\"";
            string filename = "\"acclient\"";
            string extension = "\"exe\"";
            string qry = String.Format("select * from cim_logicalfile where drive={0} and filename={1} and extension={2}", drive, filename, extension);
            ManagementObjectSearcher query = new ManagementObjectSearcher(qry); ManagementObjectCollection queryCollection = query.Get();
            foreach (ManagementObject mo in queryCollection)
            {
                acs.Add(mo["Name"].ToString());
            }
            return acs;
        }
        static bool YesNo(string prompt)
        {
            while (true)
            {
                Console.WriteLine(prompt);
                var _yn = Console.ReadKey(true).KeyChar.ToString().ToUpper();
                if (_yn != "Y" && _yn != "N")
                {
                    continue;
                }
                return (_yn == "Y");
            }
        }
        static void Fix(string exeFilPath)
        {
            try
            {
                //HKEY_CURRENT_USER\Software\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers

                var compatSettings = "~ GDIDPISCALING DPIUNAWARE";

                using (RegistryKey key = Registry.CurrentUser.OpenSubKey("Software\\Microsoft\\Windows NT\\CurrentVersion\\AppCompatFlags\\Layers", true))
                {

                    bool ok = false;
                    object r = key.GetValue(exeFilPath);
                    if (r != null)
                    {
                        var g = (string)r;
                        if (g == compatSettings)
                        {
                            ok = true;
                        }
                    }
                    if (!ok)
                    {
                        key.SetValue(exeFilPath, compatSettings);
                        Console.WriteLine($"Applied DPI fix for: {exeFilPath}");
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Failed to apply DPI fix for: {exeFilPath} because: {ex.Message}");
            }
        }
    }
}
